var SHA256 = require('crypto-js/sha256')
var HmacSHA256 = require('crypto-js/hmac-sha256')

var accessKey = null
var secretKey = null
var sortKey = null
var tableName = null
var region = null

function init(config) {
  accessKey = config.accessKey
  secretKey = config.secretKey
  region = config.region
}

function getSignatureKey(key, dateStamp, regionName, serviceName) {
  var keyDate = HmacSHA256(dateStamp, 'AWS4' + key)
  var keyRegion = HmacSHA256(regionName, keyDate)
  var keyService = HmacSHA256(serviceName, keyRegion)
  var keySigning = HmacSHA256('aws4_request', keyService)
  return keySigning
}

function signAndSendRequest(region, target, body) {
  const { amzdate, datestamp } = getDates()

  const service = 'dynamodb'
  const host = `${service}.${region}.amazonaws.com`
  const endpoint = `https://${host}`

  const method = 'POST'
  const canonicalUri = '/'
  const canonicalQuerystring = ''
  const canonicalHeaders = `host:${host}\n` + `x-amz-date:${amzdate}\n`
  const signedHeaders = 'host;x-amz-date'
  const payloadHash = SHA256(body)
  const algorithm = 'AWS4-HMAC-SHA256'

  const canonicalRequest =
    method +
    '\n' +
    canonicalUri +
    '\n' +
    canonicalQuerystring +
    '\n' +
    canonicalHeaders +
    '\n' +
    signedHeaders +
    '\n' +
    payloadHash
  const credentialScope =
    datestamp + '/' + region + '/' + service + '/' + 'aws4_request'
  const stringToSign =
    algorithm +
    '\n' +
    amzdate +
    '\n' +
    credentialScope +
    '\n' +
    SHA256(canonicalRequest)

  const signingKey = getSignatureKey(secretKey, datestamp, region, service)
  const signature = HmacSHA256(stringToSign, signingKey)

  const authorizationHeader =
    algorithm +
    ' ' +
    'Credential=' +
    accessKey +
    '/' +
    credentialScope +
    ', ' +
    'SignedHeaders=' +
    signedHeaders +
    ', ' +
    'Signature=' +
    signature

  const params = {
    method: method,
    headers: {
      'Accept-Encoding': 'identity',
      'Content-Type': 'application/x-amz-json-1.0',
      Authorization: authorizationHeader,
      'X-Amz-Date': amzdate,
      'X-Amz-Target': `DynamoDB_20120810.${target}`,
    },
    body: body,
  }

  return fetch(endpoint, params)
}

async function getDynamoItem(pkey) {
  const requestObject = {
    TableName: 'chain-reaction',
    Key: {},
  }
  requestObject.Key['game_id'] = { S: pkey }
  // requestObject.Key[sortKey] = { N: '' + skey }

  const request = JSON.stringify(requestObject)

  const firstRegion = region // TODO: assign a primary read/write region

  const promise = signAndSendRequest(firstRegion, 'GetItem', request)
    .then(rsp => {
      return { response: rsp }
    })
    .catch(ex => ({
      response: { ok: false, status: 999, statusText: ex.toString() },
    }))

  let { response } = await promise
  let responseBody
  if (response.ok) {
    responseBody = await response.json()
    console.log('dynamo', responseBody)
    if (responseBody.Item) {
      return {
        success: true,
        data: responseBody,
        message: '',
        code: 200,
      }
    } else {
      return {
        success: false,
        data: {},
        message: 'Not Found',
        code: 404,
        test: responseBody,
        id: 1,
      }
    }
  } else {
    return {
      success: false,
      data: {},
      message: 'Server Error',
      code: 500,
      test: responseBody,
      id: 2,
    }
  }
}

async function putDynamoItem(data, condition) {
  const requestObject = {
    TableName: 'chain-reaction',
    ConditionExpression: condition,
    Item: data,
  }

  const request = JSON.stringify(requestObject)

  const firstRegion = region // TODO: assign a primary read/write region

  const promise = signAndSendRequest(firstRegion, 'PutItem', request)
    .then(rsp => {
      return { response: rsp }
    })
    .catch(ex => ({
      response: { ok: false, status: 999, statusText: ex.toString() },
    }))

  var { response } = await promise
  let responseBody = await response.json()
  console.log('dynamo', responseBody)
  if (response.ok) {
    return {
      success: true,
      data: {},
      message: '',
    }
  } else if (
    responseBody &&
    responseBody.message &&
    responseBody.message === 'The conditional request failed'
  ) {
    return {
      success: false,
      data: {},
      message: 'game_id' + ' Already Exists',
      retry: true,
    }
  } else {
    return {
      success: false,
      data: {},
      message: 'Server Error',
      test: responseBody,
      id: 3,
      condition: condition,
      data: data,
    }
  }
}

async function updateDynamoItem(key, data, version) {
  let expString = 'SET '
  let expAttrValues = {}
  let expAttrNames = {}
  for (let key in data) {
    expString += '#' + key + ' = :' + key + ', '
    expAttrValues[':' + key] = data[key]
    expAttrNames['#' + key] = key
  }
  expString = expString.substr(0, expString.length - 2)
  let conditionExpStr
  if (version) {
    expAttrValues[':vers'] = { S: version }
    conditionExpStr = 'attribute_exists(' + 'game_id' + ') AND version = :vers'
  } else {
    conditionExpStr = 'attribute_exists(' + 'game_id' + ')'
  }

  const requestObject = {
    Key: key,
    TableName: 'chain-reaction',
    UpdateExpression: expString,
    ExpressionAttributeValues: expAttrValues,
    ExpressionAttributeNames: expAttrNames,
    ConditionExpression: conditionExpStr,
  }
  console.log(requestObject)

  const request = JSON.stringify(requestObject)

  const firstRegion = region // TODO: assign a primary read/write region
  const promise = signAndSendRequest(firstRegion, 'UpdateItem', request)
    .then(rsp => {
      return { response: rsp }
    })
    .catch(ex => ({
      response: { ok: false, status: 999, statusText: ex.toString() },
    }))

  var { response } = await promise
  let responseBody = await response.json()
  console.log('dynamo', responseBody)
  if (response.ok) {
    return {
      success: true,
      data: {},
      message: '',
    }
  } else if (
    responseBody &&
    responseBody.message &&
    responseBody.message === 'The conditional request failed'
  ) {
    return {
      success: false,
      data: {},
      message: 'Server Error',
      test: responseBody,
      id: 4,
    }
  } else {
    return {
      success: false,
      data: {},
      message: 'Server Error',
      test: responseBody,
      id: 5,
      key: key,
      data: data,
    }
  }
}

async function userGetDynamoItem(pkey) {
  const requestObject = {
    TableName: 'user',
    Key: {},
  }
  requestObject.Key['user_id'] = { S: pkey }
  // requestObject.Key[sortKey] = { N: '' + skey }

  const request = JSON.stringify(requestObject)

  const firstRegion = region // TODO: assign a primary read/write region

  const promise = signAndSendRequest(firstRegion, 'GetItem', request)
    .then(rsp => {
      return { response: rsp }
    })
    .catch(ex => ({
      response: { ok: false, status: 999, statusText: ex.toString() },
    }))

  let { response } = await promise
  let responseBody
  if (response.ok) {
    responseBody = await response.json()
    console.log('dynamo', responseBody)
    if (responseBody.Item) {
      return {
        success: true,
        data: responseBody,
        message: '',
        code: 200,
      }
    } else {
      return {
        success: false,
        data: {},
        message: 'Not Found',
        code: 404,
        test: responseBody,
        id: 1,
      }
    }
  } else {
    return {
      success: false,
      data: {},
      message: 'Server Error',
      code: 500,
      test: responseBody,
      id: 2,
    }
  }
}

async function userPutDynamoItem(data, condition) {
  const requestObject = {
    TableName: 'user',
    ConditionExpression: condition,
    Item: data,
  }

  const request = JSON.stringify(requestObject)

  const firstRegion = region // TODO: assign a primary read/write region

  const promise = signAndSendRequest(firstRegion, 'PutItem', request)
    .then(rsp => {
      return { response: rsp }
    })
    .catch(ex => ({
      response: { ok: false, status: 999, statusText: ex.toString() },
    }))

  var { response } = await promise
  let responseBody = await response.json()
  console.log('dynamo', responseBody)
  if (response.ok) {
    return {
      success: true,
      data: {},
      message: '',
    }
  } else if (
    responseBody &&
    responseBody.message &&
    responseBody.message === 'The conditional request failed'
  ) {
    return {
      success: false,
      data: {},
      message: 'user_id' + ' Already Exists',
      retry: true,
    }
  } else {
    return {
      success: false,
      data: {},
      message: 'Server Error',
      test: responseBody,
      id: 3,
      condition: condition,
      data: data,
    }
  }
}

async function userUpdateDynamoItem(key, data, version) {
  let expString = 'SET '
  let expAttrValues = {}
  let expAttrNames = {}
  for (let key in data) {
    expString += '#' + key + ' = :' + key + ', '
    expAttrValues[':' + key] = data[key]
    expAttrNames['#' + key] = key
  }
  expString = expString.substr(0, expString.length - 2)
  let conditionExpStr
  if (version) {
    expAttrValues[':vers'] = { S: version }
    conditionExpStr = 'attribute_exists(' + 'user_id' + ') AND version = :vers'
  } else {
    conditionExpStr = 'attribute_exists(' + 'user_id' + ')'
  }

  const requestObject = {
    Key: key,
    TableName: 'user',
    UpdateExpression: expString,
    ExpressionAttributeValues: expAttrValues,
    ExpressionAttributeNames: expAttrNames,
    ConditionExpression: conditionExpStr,
  }
  console.log(requestObject)

  const request = JSON.stringify(requestObject)

  const firstRegion = region // TODO: assign a primary read/write region
  const promise = signAndSendRequest(firstRegion, 'UpdateItem', request)
    .then(rsp => {
      return { response: rsp }
    })
    .catch(ex => ({
      response: { ok: false, status: 999, statusText: ex.toString() },
    }))

  var { response } = await promise
  let responseBody = await response.json()
  console.log('dynamo', responseBody)
  if (response.ok) {
    return {
      success: true,
      data: {},
      message: '',
    }
  } else if (
    responseBody &&
    responseBody.message &&
    responseBody.message === 'The conditional request failed'
  ) {
    return {
      success: false,
      data: {},
      message: 'Server Error',
      test: responseBody,
      id: 4,
    }
  } else {
    return {
      success: false,
      data: {},
      message: 'Server Error',
      test: responseBody,
      id: 5,
      key: key,
      data: data,
    }
  }
}

function pad(number) {
  if (number < 10) {
    return '0' + number
  }
  return number
}

function getDates() {
  const date = new Date()

  const amzdate =
    date.getUTCFullYear() +
    '' +
    pad(date.getUTCMonth() + 1) +
    '' +
    pad(date.getUTCDate()) +
    'T' +
    pad(date.getUTCHours()) +
    '' +
    pad(date.getUTCMinutes()) +
    '' +
    pad(date.getUTCSeconds()) +
    'Z'

  const datestamp =
    date.getUTCFullYear() +
    '' +
    pad(date.getUTCMonth() + 1) +
    '' +
    pad(date.getUTCDate())

  return { amzdate, datestamp }
}

async function getItem(key) {
  let promise = getDynamoItem(key)
  let result = await promise
  return result
}

async function putItem(data, condition) {
  let promise = putDynamoItem(data, condition)
  let result = await promise
  return result
}

async function updateItem(key, data) {
  let promise = updateDynamoItem(key, data)
  let result = await promise
  return result
}

async function updateItemWithVC(key, data, version) {
  let promise = updateDynamoItem(key, data, version)
  let result = await promise
  return result
}

async function userGetItem(key) {
  let promise = userGetDynamoItem(key)
  let result = await promise
  return result
}

async function userPutItem(data, condition) {
  let promise = userPutDynamoItem(data, condition)
  let result = await promise
  return result
}

async function userUpdateItem(key, data) {
  let promise = userUpdateDynamoItem(key, data)
  let result = await promise
  return result
}

async function userUpdateItemWithVC(key, data, version) {
  let promise = userUpdateDynamoItem(key, data, version)
  let result = await promise
  return result
}
exports.init = init
exports.get = getItem
exports.put = putItem
exports.update = updateItem
exports.updateVC = updateItemWithVC
exports.userGet = userGetItem
exports.userPut = userPutItem
exports.userUpdate = userUpdateItem
exports.userUpdateVC = userUpdateItemWithVC
