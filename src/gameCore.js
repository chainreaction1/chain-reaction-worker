const TURN_TIME = 20;
let p1 = [],
  p2 = [],
  GRID_SIZE,
  turnTime = TURN_TIME;

const handleClick = (turn, pp1, pp2, cords, grid_size) => {
  GRID_SIZE = grid_size;
  p1 = pp1;
  p2 = pp2;
  const clickedNumX = cords[0];
  const clickedNumY = cords[1];

  let tempP;
  let turnArr = turn === 1 ? p1 : p2;
  let otherTurnArr = turn === 2 ? p1 : p2;

  if (otherTurnArr[clickedNumX][clickedNumY] > 0) {
    return { success: false };
  }
  /**Edge and Corner Cases */
  if (
    (clickedNumX === 0 && clickedNumY === 0) ||
    (clickedNumX === GRID_SIZE - 1 && clickedNumY === 0) ||
    (clickedNumX === 0 && clickedNumY === GRID_SIZE - 1) ||
    (clickedNumX === GRID_SIZE - 1 && clickedNumY === GRID_SIZE - 1)
  ) {
    if (turnArr[clickedNumX][clickedNumY] > 0) {
      react(clickedNumX, clickedNumY, turn);
    } else {
      tempP = JSON.parse(JSON.stringify(turnArr));
      tempP[clickedNumX][clickedNumY]++;
      if (turn === 1) {
        p1 = tempP;
      } else {
        p2 = tempP;
      }
      turn = turn === 1 ? 2 : 1;
    }
  } else if (
    clickedNumX === 0 ||
    clickedNumY === 0 ||
    clickedNumX === GRID_SIZE - 1 ||
    clickedNumY === GRID_SIZE - 1
  ) {
    if (turnArr[clickedNumX][clickedNumY] > 1) {
      react(clickedNumX, clickedNumY, turn);
    } else {
      tempP = JSON.parse(JSON.stringify(turnArr));
      tempP[clickedNumX][clickedNumY]++;
      if (turn === 1) {
        p1 = tempP;
      } else {
        p2 = tempP;
      }
      turn = turn === 1 ? 2 : 1;
    }
  } else {
    if (turnArr[clickedNumX][clickedNumY] > 2) {
      react(clickedNumX, clickedNumY, turn);
    } else {
      tempP = JSON.parse(JSON.stringify(turnArr));
      tempP[clickedNumX][clickedNumY]++;
      if (turn === 1) {
        p1 = tempP;
      } else {
        p2 = tempP;
      }
      turn = turn === 1 ? 2 : 1;
    }
  }
  let p1Exists = false,
    p2Exists = false,
    winner = null;

  for (let x = 0; x < GRID_SIZE; x++) {
    for (let y = 0; y < GRID_SIZE; y++) {
      if (p1[x][y] >= 1) {
        p1Exists = true;
      }
      if (p2[x][y] >= 1) {
        p2Exists = true;
      }
    }
  }
  console.log(p1, "\n", p2);

  if (!p1Exists || !p2Exists) {
    if (!p1Exists) {
      winner = 2;
    } else {
      winner = 1;
    }
  }
  console.log("winner", winner);

  return { success: true, p1: p1, p2: p2, winner: winner, turnTime: turnTime };
};

react = (x, y, player) => {
  let repeatLoop = true;
  let testing = 30;
  let finalMatrixP1 = [];
  let finalMatrixP2 = [];
  let movement = [];
  let loopCounter = 0;

  let activeMatrix =
    player === 1
      ? JSON.parse(JSON.stringify(p1))
      : JSON.parse(JSON.stringify(p2));
  let otherMatrix =
    player === 1
      ? JSON.parse(JSON.stringify(p2))
      : JSON.parse(JSON.stringify(p1));

  activeMatrix[x][y]++;
  let activeMatrix1, otherMatrix1;
  while (testing > 0 && repeatLoop) {
    testing--;
    repeatLoop = false;
    movement[loopCounter] = [];
    activeMatrix1 = JSON.parse(JSON.stringify(activeMatrix));
    otherMatrix1 = JSON.parse(JSON.stringify(otherMatrix));
    for (let xx = 0; xx < GRID_SIZE; xx++) {
      for (let yy = 0; yy < GRID_SIZE; yy++) {
        if (
          (xx === 0 && yy === 0) ||
          (xx === GRID_SIZE - 1 && yy === 0) ||
          (xx === 0 && yy === GRID_SIZE - 1) ||
          (xx === GRID_SIZE - 1 && yy === GRID_SIZE - 1)
        ) {
          // corner
          if (activeMatrix[xx][yy] > 1) {
            repeatLoop = true;
            activeMatrix1[xx][yy] = 0;
            if (xx === 0 && yy === 0) {
              movement[loopCounter].push([xx, yy, "R"], [xx, yy, "D"]);
              activeMatrix1[xx + 1][yy] =
                activeMatrix[xx + 1][yy] + otherMatrix[xx + 1][yy] + 1;
              otherMatrix1[xx + 1][yy] = 0;
              activeMatrix1[xx][yy + 1] =
                activeMatrix[xx][yy + 1] + otherMatrix[xx][yy + 1] + 1;
              otherMatrix1[xx][yy + 1] = 0;
            } else if (xx === GRID_SIZE - 1 && yy === 0) {
              movement[loopCounter].push([xx, yy, "L"], [xx, yy, "D"]);
              activeMatrix1[xx - 1][yy] =
                activeMatrix[xx - 1][yy] + otherMatrix[xx - 1][yy] + 1;
              otherMatrix1[xx - 1][yy] = 0;
              activeMatrix1[xx][yy + 1] =
                activeMatrix[xx][yy + 1] + otherMatrix[xx][yy + 1] + 1;
              otherMatrix1[xx][yy + 1] = 0;
            } else if (xx === 0 && yy === GRID_SIZE - 1) {
              movement[loopCounter].push([xx, yy, "R"], [xx, yy, "U"]);
              activeMatrix1[xx + 1][yy] =
                activeMatrix[xx + 1][yy] + otherMatrix[xx + 1][yy] + 1;
              otherMatrix1[xx + 1][yy] = 0;
              activeMatrix1[xx][yy - 1] =
                activeMatrix[xx][yy - 1] + otherMatrix[xx][yy - 1] + 1;
              otherMatrix1[xx][yy - 1] = 0;
            } else {
              movement[loopCounter].push([xx, yy, "L"], [xx, yy, "U"]);
              activeMatrix1[xx - 1][yy] =
                activeMatrix[xx - 1][yy] + otherMatrix[xx - 1][yy] + 1;
              otherMatrix1[xx - 1][yy] = 0;
              activeMatrix1[xx][yy - 1] =
                activeMatrix[xx][yy - 1] + otherMatrix[xx][yy - 1] + 1;
              otherMatrix1[xx][yy - 1] = 0;
            }
          }
        } else if (
          xx === 0 ||
          yy === 0 ||
          xx === GRID_SIZE - 1 ||
          yy === GRID_SIZE - 1
        ) {
          // edge
          // console.log("REACT-EDGE");
          if (activeMatrix[xx][yy] > 2) {
            repeatLoop = true;
            activeMatrix1[xx][yy] = 0;
            if (xx === 0) {
              movement[loopCounter].push(
                [xx, yy, "R"],
                [xx, yy, "U"],
                [xx, yy, "D"]
              );
              activeMatrix1[xx + 1][yy] =
                activeMatrix[xx + 1][yy] + otherMatrix[xx + 1][yy] + 1;
              otherMatrix1[xx + 1][yy] = 0;
              activeMatrix1[xx][yy - 1] =
                activeMatrix[xx][yy - 1] + otherMatrix[xx][yy - 1] + 1;
              otherMatrix1[xx][yy - 1] = 0;
              activeMatrix1[xx][yy + 1] =
                activeMatrix[xx][yy + 1] + otherMatrix[xx][yy + 1] + 1;
              otherMatrix1[xx][yy + 1] = 0;
            } else if (yy === 0) {
              movement[loopCounter].push(
                [xx, yy, "R"],
                [xx, yy, "L"],
                [xx, yy, "D"]
              );
              activeMatrix1[xx + 1][yy] =
                activeMatrix[xx + 1][yy] + otherMatrix[xx + 1][yy] + 1;
              otherMatrix1[xx + 1][yy] = 0;
              activeMatrix1[xx - 1][yy] =
                activeMatrix[xx - 1][yy] + otherMatrix[xx - 1][yy] + 1;
              otherMatrix1[xx - 1][yy] = 0;
              activeMatrix1[xx][yy + 1] =
                activeMatrix[xx][yy + 1] + otherMatrix[xx][yy + 1] + 1;
              otherMatrix1[xx][yy + 1] = 0;
            } else if (xx === GRID_SIZE - 1) {
              movement[loopCounter].push(
                [xx, yy, "L"],
                [xx, yy, "U"],
                [xx, yy, "D"]
              );
              activeMatrix1[xx - 1][yy] =
                activeMatrix[xx - 1][yy] + otherMatrix[xx - 1][yy] + 1;
              otherMatrix1[xx - 1][yy] = 0;
              activeMatrix1[xx][yy - 1] =
                activeMatrix[xx][yy - 1] + otherMatrix[xx][yy - 1] + 1;
              otherMatrix1[xx][yy - 1] = 0;
              activeMatrix1[xx][yy + 1] =
                activeMatrix[xx][yy + 1] + otherMatrix[xx][yy + 1] + 1;
              otherMatrix1[xx][yy + 1] = 0;
            } else {
              movement[loopCounter].push(
                [xx, yy, "R"],
                [xx, yy, "L"],
                [xx, yy, "U"]
              );
              activeMatrix1[xx + 1][yy] =
                activeMatrix[xx + 1][yy] + otherMatrix[xx + 1][yy] + 1;
              otherMatrix1[xx + 1][yy] = 0;
              activeMatrix1[xx - 1][yy] =
                activeMatrix[xx - 1][yy] + otherMatrix[xx - 1][yy] + 1;
              otherMatrix1[xx - 1][yy] = 0;
              activeMatrix1[xx][yy - 1] =
                activeMatrix[xx][yy - 1] + otherMatrix[xx][yy - 1] + 1;
              otherMatrix1[xx][yy - 1] = 0;
            }
          }
        } else {
          // other
          // console.log("REACT-OTHER");
          if (activeMatrix[xx][yy] > 3) {
            repeatLoop = true;
            activeMatrix1[xx][yy] = 0;
            movement[loopCounter].push(
              [xx, yy, "R"],
              [xx, yy, "L"],
              [xx, yy, "U"],
              [xx, yy, "D"]
            );
            activeMatrix1[xx + 1][yy] =
              activeMatrix[xx + 1][yy] + otherMatrix[xx + 1][yy] + 1;
            otherMatrix1[xx + 1][yy] = 0;
            activeMatrix1[xx - 1][yy] =
              activeMatrix[xx - 1][yy] + otherMatrix[xx - 1][yy] + 1;
            otherMatrix1[xx - 1][yy] = 0;
            activeMatrix1[xx][yy - 1] =
              activeMatrix[xx][yy - 1] + otherMatrix[xx][yy - 1] + 1;
            otherMatrix1[xx][yy - 1] = 0;
            activeMatrix1[xx][yy + 1] =
              activeMatrix[xx][yy + 1] + otherMatrix[xx][yy + 1] + 1;
            otherMatrix1[xx][yy + 1] = 0;
          }
        }
      }
    }

    activeMatrix = activeMatrix1;
    otherMatrix = otherMatrix1;
    turnTime += 0.4;
    loopCounter++;
    // enter into finalMatrix here
    if (repeatLoop && player === 1) {
      finalMatrixP1 = JSON.parse(JSON.stringify(activeMatrix));
      finalMatrixP2 = JSON.parse(JSON.stringify(otherMatrix));
    } else if (repeatLoop && player === 2) {
      finalMatrixP2 = JSON.parse(JSON.stringify(activeMatrix));
      finalMatrixP1 = JSON.parse(JSON.stringify(otherMatrix));
    }
  }

  p1 = finalMatrixP1;
  p2 = finalMatrixP2;
};

exports.handleClick = handleClick;
