import dynamo from "./dynamo";
const nanoid = require("nanoid/generate");
const moment = require("moment");
const bcrypt = require("bcryptjs");
import core from "./gameCore";

const TURN_TIME = 20;
const GRID_SIZE = 6;
const DUO_POINTS = 5;
let HEADERS = {
  "content-type": "text/plain",
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Headers":
    "Cache-Control, Content-Type, session_id, access-control-allow-origin, access-control-allow-headers",
  "access-control-max-age": "86400",
};

dynamo.init({
  accessKey: "AKIA2L4O6VIEEPT4MW56",
  secretKey: "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",
  primaryKey: "user_id",
  region: "us-east-2",
});

addEventListener("fetch", (event) => {
  event.respondWith(handleRequest(event));
});

async function handleRequest(event) {
  let request = event.request;
  let url = new URL(request.url);

  if (request.method === "OPTIONS") {
    return new Response(
      {},
      {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Headers":
            "Cache-Control, Content-Type, cache-override-nounce, session_id, access-control-allow-origin, access-control-allow-headers",
        },
        status: 200,
        body: {},
      }
    );
  }
  // if (url.hostname.substr(0, 3) === 'api') {

  if (request.method === "GET") {
    switch (url.pathname) {
      case "/getGameInfo": {
        let gameIdStr = url.searchParams.get("id");
        let promise = getPublicGameInfo(gameIdStr);
        event.waitUntil(promise);
        let result = await promise;
        return new Response(JSON.stringify(result.data), {
          headers: result.headers,
          status: result.status,
        });
      }
      default:
        return new Response(null, {
          headers: HEADERS,
          status: 400,
        });
    }
  } else if (request.method === "POST") {
    let requestBody = await event.request.json();

    switch (url.pathname) {
      case "/createGame": {
        let promise = createGame(requestBody.name);
        event.waitUntil(promise);
        let result = await promise;
        return new Response(JSON.stringify(result.data), {
          headers: result.headers,
          status: result.status,
        });
      }
      case "/click": {
        let promise = handleClick(
          requestBody.id,
          requestBody.secret,
          requestBody.cords
        );
        event.waitUntil(promise);
        let result = await promise;
        return new Response(JSON.stringify(result.data), {
          headers: result.headers,
          status: result.status,
        });
      }
      case "/joinGame": {
        let promise = joinGame(requestBody.id, requestBody.name);
        event.waitUntil(promise);
        let result = await promise;
        return new Response(JSON.stringify(result.data), {
          headers: result.headers,
          status: result.status,
        });
      }
      case "/login": {
        let promise = login(requestBody.userId, requestBody.password);
        event.waitUntil(promise);
        let result = await promise;
        return new Response(JSON.stringify(result.data), {
          headers: result.headers,
          status: result.status,
        });
      }
      case "/register": {
        let promise = register(requestBody.userId, requestBody.password);
        event.waitUntil(promise);
        let result = await promise;
        return new Response(JSON.stringify(result.data), {
          headers: result.headers,
          status: result.status,
        });
      }
      case "/match": {
        let promise = match(requestBody.userId, requestBody.sessionId);
        event.waitUntil(promise);
        let result = await promise;
        return new Response(JSON.stringify(result.data), {
          headers: result.headers,
          status: result.status,
        });
      }
      case "/sendMessage": {
        let promise = sendMessage(
          requestBody.gameId,
          requestBody.secret,
          requestBody.message
        );
        event.waitUntil(promise);
        let result = await promise;
        return new Response(JSON.stringify(result.data), {
          headers: result.headers,
          status: result.status,
        });
      }
      default:
        return new Response(null, {
          headers: HEADERS,
          status: 400,
        });
    }
  } else {
    console.log("INVALID METHOD");
    return new Response(
      {},
      {
        headers: HEADERS,
        status: 200,
        body: {},
      }
    );
  }
}

const createGame = async (name) => {
  name = name.replace(/[^a-zA-Z0-9]/, "");
  if (!name || name.length < 1) {
    return {
      data: {
        success: false,
        message: "Invalid Name",
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  name = String(name);
  name = name[0].toUpperCase() + name.slice(1).toLowerCase();
  let ttl = moment()
    .add(1, "h")
    .utc()
    .unix()
    .toString();
  let result;
  let id, userSecret;
  userSecret = nanoid(
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
    16
  );
  for (let i = 0; i < 5; i++) {
    id = nanoid("1234567890", 6);
    result = await dynamo.put(
      {
        game_id: {
          S: id,
        },
        user1_name: {
          S: name,
        },
        user1_secret: {
          S: userSecret,
        },
        ttl: {
          N: ttl,
        },
      },
      "attribute_not_exists(game_id)"
    );
    if (!result.retry) {
      break;
    }
  }
  delete result.retry;
  if (result.success && result.data) {
    result.data.gameId = id;
    result.data.secret = userSecret;
  }

  return { data: result, headers: HEADERS };
};

const joinGame = async (id, name) => {
  name = name.replace(/[^a-zA-Z0-9]/, "");
  if (id && id.substr(-1) === "/") {
    id = id.substr(0, id.length - 1);
  }
  if (!id || id.length !== 6) {
    return {
      data: {
        success: false,
        message: "Invalid Game Code",
      },
      headers: HEADERS,
    };
  }

  if (!name || name.length < 1) {
    return {
      data: {
        success: false,
        message: "Invalid Name",
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  name = String(name);
  name = name[0].toUpperCase() + name.slice(1).toLowerCase();

  let result = await dynamo.get(id);
  console.log(result);
  if (!result.success) {
    if (result.code === 404) {
      return {
        data: {
          success: false,
          message: "Invalid Game Code",
          code: 404,
        },
        headers: HEADERS,
      };
    } else {
      return { data: result, headers: HEADERS };
    }
  } else if (
    result &&
    result.data &&
    result.data.Item &&
    (!result.data.Item.user1_secret ||
      !result.data.Item.user1_secret.S ||
      result.data.Item.user1_secret.S.length !== 16 ||
      !result.data.Item.user1_name ||
      !result.data.Item.user1_name.S ||
      result.data.Item.user1_name.S.length < 1)
  ) {
    console.log("Invalid Game");
    return {
      data: {
        success: false,
        message: "Cannot Join Game",
      },
      headers: HEADERS,
    };
  } else if (
    result &&
    result.data &&
    result.data.Item &&
    result.data.Item.user2_secret &&
    result.data.Item.user2_secret.S &&
    result.data.Item.user2_secret.S.length > 0
  ) {
    console.log("Game is already in progress");
    return {
      data: {
        success: false,
        message: "Cannot Join Game",
      },
      headers: HEADERS,
    };
  } else {
    let userSecret = nanoid(
      "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
      16
    );
    let ttl = moment()
      .add(1, "h")
      .utc()
      .unix()
      .toString();
    let p1 = JSON.stringify(
      Array(GRID_SIZE)
        .fill(0)
        .map(() => Array(GRID_SIZE).fill(0))
    );
    let createTurnTime = moment()
      .add(TURN_TIME, "seconds")
      .utc()
      .valueOf()
      .toString();
    let createTurnTimePadded = moment()
      .add(TURN_TIME - 2, "seconds")
      .utc()
      .valueOf()
      .toString();
    let result1 = await dynamo.update(
      {
        game_id: {
          S: id,
        },
      },
      {
        user2_secret: {
          S: userSecret,
        },
        user2_name: {
          S: name,
        },
        ttl: {
          N: ttl,
        },
        turn: {
          N: "1",
        },
        p1: {
          S: String(p1),
        },
        p2: {
          S: String(p1),
        },
        updated: {
          N: moment()
            .utc()
            .unix()
            .toString(),
        },
        sequence_id: {
          N: "0",
        },
        turnTime: {
          N: createTurnTime,
        },
      }
    );
    if (result1.success && result1.data) {
      result1.data.secret = userSecret;
      result1.data.opponent_name = result.data.Item.user1_name.S;
      result1.data.turnTime = createTurnTimePadded;
    }
    return { data: result1, headers: HEADERS };
  }
};

const handleClick = async (id, secret, cords) => {
  if (
    id &&
    (id.length === 6 || id.length === 12) &&
    secret &&
    secret.length === 16 &&
    cords
  ) {
    let result = await dynamo.get(id);
    console.log(result);
    if (result.success) {
      if (
        result.data.Item.user1_secret &&
        result.data.Item.user1_secret.S &&
        result.data.Item.user1_secret.S.length === 16 &&
        result.data.Item.user2_secret &&
        result.data.Item.user2_secret.S &&
        result.data.Item.user2_secret.S.length === 16 &&
        result.data.Item.turn &&
        result.data.Item.turn.N
      ) {
        if (
          result.data.Item.winner &&
          result.data.Item.winner.N &&
          (parseInt(result.data.Item.winner.N) === 1 ||
            parseInt(result.data.Item.winner.N) === 2)
        ) {
          return {
            data: {
              success: false,
              message: "Game Already Completed",
              data: {
                winner: parseInt(result.data.Item.winner.N),
                winReason:
                  (result.data.Item.winReason &&
                    result.data.Item.winReason.S) ||
                  null,
              },
            },
            headers: HEADERS,
          };
        }
        let p1 = JSON.parse(result.data.Item.p1.S);
        let p2 = JSON.parse(result.data.Item.p2.S);
        if (
          cords.length !== 2 ||
          cords[0] > GRID_SIZE - 1 ||
          cords[1] > GRID_SIZE - 1 ||
          cords[0] < 0 ||
          cords[1] < 0
        ) {
          return {
            data: {
              success: false,
              message: "Invalid Data",
            },
            headers: HEADERS,
          };
        }
        if (
          secret === result.data.Item.user1_secret.S &&
          result.data.Item.turn.N === "1"
        ) {
          //user 1
          let ttl = moment()
            .add(1, "h")
            .utc()
            .unix()
            .toString();
          let clickResult = core.handleClick(1, p1, p2, cords, GRID_SIZE);
          let clickTurnTime = moment()
            .add(clickResult.turnTime, "seconds")
            .utc()
            .valueOf()
            .toString();
          let updatedP1;
          let updatedP2;
          if (clickResult.success) {
            updatedP1 = JSON.stringify(clickResult.p1);
            updatedP2 = JSON.stringify(clickResult.p2);
          } else {
            return {
              data: {
                success: false,
                message: "Invalid Data",
              },
              headers: HEADERS,
            };
          }
          let updateBody = {
            ttl: {
              N: ttl,
            },
            turn: {
              N: "2",
            },
            p1: {
              S: String(updatedP1),
            },
            p2: {
              S: String(updatedP2),
            },
            updated: {
              N: moment()
                .utc()
                .unix()
                .toString(),
            },
            sequence_id: {
              N: String(parseInt(result.data.Item.sequence_id.N) + 1),
            },
            cords: {
              S: String(JSON.stringify(cords)),
            },
            turnTime: {
              N: clickTurnTime,
            },
            inactive_1_count: { N: "0" },
          };
          if (
            clickResult.winner &&
            result.data.Item &&
            result.data.Item.sequence_id &&
            result.data.Item.sequence_id.N &&
            parseInt(result.data.Item.sequence_id.N) > 1
          ) {
            updateBody.winner = { N: String(clickResult.winner) };
            if (
              String(clickResult.winner) === "1" &&
              result.data.Item.user1_name &&
              result.data.Item.user1_name.S &&
              result.data.Item.user1_session_id &&
              result.data.Item.user1_session_id.S
            ) {
              await updateUserPoints(
                result.data.Item.user1_name.S,
                result.data.Item.user1_session_id.S,
                DUO_POINTS
              );
            } else if (
              String(clickResult.winner) === "2" &&
              result.data.Item.user2_name &&
              result.data.Item.user2_name.S &&
              result.data.Item.user2_session_id &&
              result.data.Item.user2_session_id.S
            ) {
              await updateUserPoints(
                result.data.Item.user2_name.S,
                result.data.Item.user2_session_id.S,
                DUO_POINTS
              );
            }
          }
          let updateResult = await dynamo.update(
            {
              game_id: {
                S: id,
              },
            },
            updateBody
          );
          if (updateResult.success) {
            return {
              data: {
                updatedP1: updatedP1,
                updatedP2: updatedP2,
                updatedTurn: 2,
                turnTime: clickTurnTime,
              },
              headers: HEADERS,
            };
          } else {
            return {
              data: {
                success: false,
                message: "Server Error",
              },
              headers: HEADERS,
            };
          }
        } else if (
          secret === result.data.Item.user2_secret.S &&
          result.data.Item.turn.N === "2"
        ) {
          //user2
          let ttl = moment()
            .add(1, "h")
            .utc()
            .unix()
            .toString();
          let clickResult = core.handleClick(2, p1, p2, cords, GRID_SIZE);
          let clickTurnTime = moment()
            .add(clickResult.turnTime, "seconds")
            .utc()
            .valueOf()
            .toString();
          let updatedP1;
          let updatedP2;
          if (clickResult.success) {
            updatedP1 = JSON.stringify(clickResult.p1);
            updatedP2 = JSON.stringify(clickResult.p2);
          } else {
            return {
              data: {
                success: false,
                message: "Invalid Data",
              },
              headers: HEADERS,
            };
          }
          let updateBody = {
            ttl: {
              N: ttl,
            },
            turn: {
              N: "1",
            },
            p1: {
              S: String(updatedP1),
            },
            p2: {
              S: String(updatedP2),
            },
            updated: {
              N: moment()
                .utc()
                .unix()
                .toString(),
            },
            sequence_id: {
              N: String(parseInt(result.data.Item.sequence_id.N) + 1),
            },
            cords: {
              S: String(JSON.stringify(cords)),
            },
            turnTime: {
              N: clickTurnTime,
            },
            inactive_2_count: { N: "0" },
          };
          if (
            clickResult.winner &&
            result.data.Item &&
            result.data.Item.sequence_id &&
            result.data.Item.sequence_id.N &&
            parseInt(result.data.Item.sequence_id.N) > 1
          ) {
            updateBody.winner = { N: String(clickResult.winner) };
            if (
              String(clickResult.winner) === "1" &&
              result.data.Item.user1_name &&
              result.data.Item.user1_name.S &&
              result.data.Item.user1_session_id &&
              result.data.Item.user1_session_id.S
            ) {
              await updateUserPoints(
                result.data.Item.user1_name.S,
                result.data.Item.user1_session_id.S,
                DUO_POINTS
              );
            } else if (
              String(clickResult.winner) === "2" &&
              result.data.Item.user2_name &&
              result.data.Item.user2_name.S &&
              result.data.Item.user2_session_id &&
              result.data.Item.user2_session_id.S
            ) {
              await updateUserPoints(
                result.data.Item.user2_name.S,
                result.data.Item.user2_session_id.S,
                DUO_POINTS
              );
            }
          }
          let updateResult = await dynamo.update(
            {
              game_id: {
                S: id,
              },
            },
            updateBody
          );
          if (updateResult.success) {
            return {
              data: {
                updatedP1: updatedP1,
                updatedP2: updatedP2,
                updatedTurn: 2,
              },
              headers: HEADERS,
            };
          } else {
            return {
              data: {
                success: false,
                message: "Server Error",
              },
              headers: HEADERS,
            };
          }
        } else {
          return {
            data: {
              success: false,
              message: "Unauthorized",
            },
            headers: HEADERS,
          };
        }
      } else {
        return {
          data: {
            success: false,
            message: "Server Error",
          },
          headers: HEADERS,
        };
      }
    } else {
      if (result.data) {
        delete result.data;
      }
      return { data: result, headers: HEADERS };
    }
  } else {
    return {
      data: {
        success: false,
        message: "Invalid Data",
      },
      headers: HEADERS,
      status: 400,
    };
  }
};

const getPublicGameInfo = async (id) => {
  if (id && id.substr(-1) === "/") {
    id = id.substr(0, id.length - 1);
  }
  if (!id || !(id.length === 6 || id.length === 12)) {
    return {
      data: {
        success: false,
        message: "Invalid Data",
      },
      headers: HEADERS,
    };
  }
  let result = await dynamo.get(id);
  let publicResult = { success: true, data: {} };
  if (result.success && result.data && result.data.Item) {
    publicResult.data.user1Name =
      (result.data.Item.user1_name && result.data.Item.user1_name.S) || null;
    publicResult.data.user2Name =
      (result.data.Item.user2_name && result.data.Item.user2_name.S) || null;
    publicResult.data.p1 =
      (result.data.Item.p1 && result.data.Item.p1.S) || null;
    publicResult.data.p2 =
      (result.data.Item.p2 && result.data.Item.p2.S) || null;
    publicResult.data.turn =
      (result.data.Item.turn && result.data.Item.turn.N) || null;
    publicResult.data.sequenceId =
      (result.data.Item.sequence_id && result.data.Item.sequence_id.N) || null;
    publicResult.data.turnTime =
      (result.data.Item.turnTime &&
        result.data.Item.turnTime.N &&
        String(parseInt(result.data.Item.turnTime.N) - 2000)) ||
      null;
    publicResult.data.user1_message =
      (result.data.Item.user1_message && result.data.Item.user1_message.S) ||
      null;
    publicResult.data.user1_message_time =
      (result.data.Item.user1_message_time &&
        result.data.Item.user1_message_time.N) ||
      null;
    publicResult.data.user2_message =
      (result.data.Item.user2_message && result.data.Item.user2_message.S) ||
      null;
    publicResult.data.user2_message_time =
      (result.data.Item.user2_message_time &&
        result.data.Item.user2_message_time.N) ||
      null;
    publicResult.data.cords =
      (result.data.Item.cords &&
        result.data.Item.cords.S &&
        JSON.parse(result.data.Item.cords.S)) ||
      null;

    if (
      result.data.Item.winner &&
      result.data.Item.winner.N &&
      (parseInt(result.data.Item.winner.N) === 1 ||
        parseInt(result.data.Item.winner.N) === 2)
    ) {
      publicResult.data.winner = parseInt(result.data.Item.winner.N);
      if (result.data.Item.winReason && result.data.Item.winReason.S) {
        publicResult.data.winReason = result.data.Item.winReason.S;
      }
    }
    let nowt = moment()
      .utc()
      .valueOf();
    let tt;
    let temptt =
      (result.data.Item.turnTime &&
        result.data.Item.turnTime.N &&
        parseInt(result.data.Item.turnTime.N)) ||
      -1;
    if (temptt > 0) {
      tt = temptt;
    }
    if (
      tt &&
      tt > 0 &&
      nowt > tt &&
      parseInt(result.data.Item.sequence_id.N) > -1 &&
      !(result.data.Item.winner && result.data.Item.winner.N)
    ) {
      let nextTurn = result.data.Item.turn.N === "1" ? "2" : "1";

      let ttl = moment()
        .add(1, "h")
        .utc()
        .unix()
        .toString();

      let clickTurnTime = moment()
        .add(TURN_TIME, "seconds")
        .utc()
        .valueOf()
        .toString();

      let updateBody = {
        ttl: {
          N: ttl,
        },
        turn: {
          N: nextTurn,
        },
        updated: {
          N: moment()
            .utc()
            .unix()
            .toString(),
        },
        sequence_id: {
          N: String(parseInt(result.data.Item.sequence_id.N) + 2),
        },
        turnTime: {
          N: clickTurnTime,
        },
      };

      if (result.data.Item.turn.N === "1") {
        if (
          result.data.Item.inactive_1_count &&
          result.data.Item.inactive_1_count.N &&
          parseInt(result.data.Item.inactive_1_count.N) > 0
        ) {
          if (
            parseInt(result.data.Item.inactive_1_count.N) > 1 &&
            !(result.data.Item.winner && result.data.Item.winner.N)
          ) {
            updateBody.winner = { N: "2" };
            updateBody.winReason = { S: "kicked for inactivity" };
            if (
              result.data.Item.user2_name &&
              result.data.Item.user2_name.S &&
              result.data.Item.user2_session_id &&
              result.data.Item.user2_session_id.S
            ) {
              await updateUserPoints(
                result.data.Item.user2_name.S,
                result.data.Item.user2_session_id.S,
                DUO_POINTS
              );
            }
          }
          updateBody.inactive_1_count = {
            N: String(parseInt(result.data.Item.inactive_1_count.N) + 1),
          };
        } else {
          updateBody.inactive_1_count = {
            N: "1",
          };
        }
      } else {
        if (
          result.data.Item.inactive_2_count &&
          result.data.Item.inactive_2_count.N &&
          parseInt(result.data.Item.inactive_2_count.N) > 0
        ) {
          if (
            parseInt(result.data.Item.inactive_2_count.N) > 1 &&
            !(result.data.Item.winner && result.data.Item.winner.N)
          ) {
            updateBody.winner = { N: "1" };
            updateBody.winReason = { S: "kicked for inactivity" };
            if (
              result.data.Item.user1_name &&
              result.data.Item.user1_name.S &&
              result.data.Item.user1_session_id &&
              result.data.Item.user1_session_id.S
            ) {
              await updateUserPoints(
                result.data.Item.user1_name.S,
                result.data.Item.user1_session_id.S,
                DUO_POINTS
              );
            }
          }
          updateBody.inactive_2_count = {
            N: String(parseInt(result.data.Item.inactive_2_count.N) + 1),
          };
        } else {
          updateBody.inactive_2_count = {
            N: "1",
          };
        }
      }

      await dynamo.update(
        {
          game_id: {
            S: id,
          },
        },
        updateBody
      );
    }

    return { data: publicResult, headers: HEADERS };
  } else if (!result.success) {
    if (result.code === 404) {
      return {
        data: {
          success: false,
          message: "Invalid Game Code",
          code: 404,
        },
        headers: HEADERS,
      };
    } else {
      return { data: result, headers: HEADERS };
    }
  } else {
    return { data: result, headers: HEADERS };
  }
};

const login = async (userId, password) => {
  userId = userId.replace(/[^a-zA-Z0-9]/, "");
  if (!(userId && userId.length >= 3 && userId.length <= 10)) {
    return {
      data: {
        success: false,
        message: "Invalid Gamer Tag",
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  if (!(password && password.length >= 8 && password.length <= 16)) {
    return {
      data: {
        success: false,
        message: "Invalid Password",
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  userId = userId[0].toUpperCase() + userId.slice(1).toLowerCase();
  let result = await dynamo.userGet(userId);
  console.log(result);
  if (!result.success) {
    if (result.code === 404) {
      return {
        data: {
          success: false,
          message: "Account Doesn't Exist",
          code: 404,
        },
        headers: HEADERS,
      };
    } else {
      return { data: result, headers: HEADERS };
    }
  } else {
    if (
      result.data &&
      result.data.Item &&
      result.data.Item.password &&
      result.data.Item.password.S &&
      result.data.Item.password.S.length > 0
    ) {
      const isRightPwd = bcrypt.compareSync(
        userId[0] + password + "#1" + userId[2],
        result.data.Item.password.S
      );
      if (isRightPwd) {
        let sessionId = nanoid(
          "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
          16
        );
        let result1 = await dynamo.userUpdate(
          {
            user_id: {
              S: userId,
            },
          },
          {
            session_id: { S: sessionId },
          }
        );
        if (result1.success && result1.data) {
          result1.data.session_id = sessionId;
          result1.data.user_id = userId;
        }
        return { data: result1, headers: HEADERS };
      } else {
        return {
          data: {
            success: false,
            message: "Incorrect Password",
            code: 200,
          },
          headers: HEADERS,
        };
      }
    }
  }
};

const register = async (userId, password) => {
  userId = userId.replace(/[^a-zA-Z0-9]/, "");
  if (!(userId && userId.length >= 3 && userId.length <= 10)) {
    return {
      data: {
        success: false,
        message: "Invalid Gamer Tag",
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  if (!(password && password.length >= 8 && password.length <= 16)) {
    return {
      data: {
        success: false,
        message: "Invalid Password",
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  userId = userId[0].toUpperCase() + userId.slice(1).toLowerCase();
  const hash = bcrypt.hashSync(userId[0] + password + "#1" + userId[2], 5);
  let result = await dynamo.userPut(
    {
      user_id: {
        S: userId,
      },
      password: {
        S: hash,
      },
      points: {
        N: "0",
      },
      created: {
        N: moment()
          .utc()
          .unix()
          .toString(),
      },
    },
    "attribute_not_exists(user_id)"
  );
  if (!result.success && result.message === "user_id Already Exists") {
    result.message = "Gamer Tag Already Taken";
  }

  return { data: result, headers: HEADERS };
};

const match = async (userId, sessionId) => {
  console.log(userId, sessionId);
  if (!(userId && userId.length >= 3 && userId.length <= 10)) {
    return {
      data: {
        success: false,
        message: "Auth Failed",
        code: 401,
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  if (!(sessionId && sessionId.length >= 8 && sessionId.length <= 16)) {
    return {
      data: {
        success: false,
        message: "Auth Failed",
        data: {},
        code: 401,
      },
      headers: HEADERS,
      status: 200,
    };
  }
  userId = userId[0].toUpperCase() + userId.slice(1).toLowerCase();
  let result = await dynamo.userGet(userId);
  if (!result.success) {
    if (result.code === 404) {
      return {
        data: {
          success: false,
          message: "Auth Failed",
          code: 401,
        },
        headers: HEADERS,
      };
    } else {
      return { data: result, headers: HEADERS };
    }
  } else {
    if (
      result.data &&
      result.data.Item &&
      result.data.Item.session_id &&
      result.data.Item.session_id.S &&
      result.data.Item.session_id.S.length > 0
    ) {
      if (sessionId === result.data.Item.session_id.S) {
        //auth pass
        let gameResult = await dynamo.get("L1");
        if (!gameResult.success) {
          if (gameResult.code === 404) {
            //no L1 entry : create new
            console.log("no L1 entry : create new");
            const user1Secret = nanoid(
              "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
              16
            );
            const targetGameId = nanoid(
              "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
              12
            );
            const version = nanoid("0123456789", 6);
            let ttl = moment()
              .add(30, "s")
              .utc()
              .unix()
              .toString();
            let putResult = await dynamo.put(
              {
                game_id: {
                  S: "L1",
                },
                user1_name: { S: userId },
                user1_secret: { S: user1Secret },
                target_game_id: { S: targetGameId },
                updated: {
                  N: moment()
                    .utc()
                    .unix()
                    .toString(),
                },
                version: { S: version },
                ttl: { N: ttl },
              },
              "attribute_not_exists(game_id)"
            );
            if (putResult.success) {
              let targetTtl = moment()
                .add(45, "m")
                .utc()
                .unix()
                .toString();
              let targetPutResult = await dynamo.put(
                {
                  game_id: {
                    S: targetGameId,
                  },
                  user1_name: { S: userId },
                  user1_secret: { S: user1Secret },
                  user1_session_id: { S: sessionId },
                  updated: {
                    N: moment()
                      .utc()
                      .unix()
                      .toString(),
                  },
                  ttl: { N: targetTtl },
                },
                "attribute_not_exists(game_id)"
              );
              if (targetPutResult.success) {
                targetPutResult.data.user1Secret = user1Secret;
                targetPutResult.data.gameId = targetGameId;
                targetPutResult.message = "added";
                return { data: targetPutResult, headers: HEADERS };
              } else {
                if (targetPutResult.message === "game_id Already Exists") {
                  targetPutResult.message = "retry";
                  targetPutResult.code = 200;
                }
                return { data: targetPutResult, headers: HEADERS };
              }
            } else {
              if (putResult.message === "game_id Already Exists") {
                putResult.message = "retry";
                putResult.code = 200;
              }
              return { data: putResult, headers: HEADERS };
            }
          } else {
            return { data: gameResult, headers: HEADERS };
          }
        } else {
          if (
            gameResult.data &&
            gameResult.data.Item &&
            gameResult.data.Item.user1_secret &&
            gameResult.data.Item.user1_secret.S &&
            gameResult.data.Item.user1_secret.S.length > 0 &&
            gameResult.data.Item.user1_secret.S !== "null" &&
            gameResult.data.Item.user1_name &&
            gameResult.data.Item.user1_name.S &&
            gameResult.data.Item.user1_name.S.length > 0 &&
            gameResult.data.Item.user1_name.S !== "null" &&
            gameResult.data.Item.target_game_id &&
            gameResult.data.Item.target_game_id.S &&
            gameResult.data.Item.target_game_id.S.length > 0 &&
            gameResult.data.Item.target_game_id.S !== "null" &&
            gameResult.data.Item.version &&
            gameResult.data.Item.version.S &&
            gameResult.data.Item.version.S.length > 0 &&
            parseInt(gameResult.data.Item.ttl.N) >
              moment()
                .utc()
                .unix()
          ) {
            if (gameResult.data.Item.user1_name.S === userId) {
              console.log(
                "user1 exists but is same as requesting opponent: re-sending poll packet"
              );
              return {
                data: {
                  success: true,
                  data: {
                    user1Secret: gameResult.data.Item.user1_secret.S,
                    gameId: gameResult.data.Item.target_game_id.S,
                  },
                  message: "added",
                },
                headers: HEADERS,
              };
            }
            //user1 exists and is waiting: team up
            console.log("user1 exists and is waiting: team up");
            const version = nanoid("0123456789", 6);
            let updateResult = await dynamo.updateVC(
              {
                game_id: {
                  S: "L1",
                },
              },
              {
                user1_secret: {
                  S: "null",
                },
                user1_name: {
                  S: "null",
                },
                target_game_id: {
                  S: "null",
                },
                ttl: {
                  N: moment()
                    .add(30, "s")
                    .utc()
                    .unix()
                    .toString(),
                },
                updated: {
                  N: moment()
                    .utc()
                    .unix()
                    .toString(),
                },
                version: { S: version },
              },
              gameResult.data.Item.version.S
            );
            if (updateResult.success) {
              let ttl = moment()
                .add(1, "h")
                .utc()
                .unix()
                .toString();
              let p1 = JSON.stringify(
                Array(GRID_SIZE)
                  .fill(0)
                  .map(() => Array(GRID_SIZE).fill(0))
              );
              let createTurnTime = moment()
                .add(TURN_TIME, "seconds")
                .utc()
                .valueOf()
                .toString();
              let createTurnTimePadded = moment()
                .add(TURN_TIME - 2, "seconds")
                .utc()
                .valueOf()
                .toString();
              const user2Secret = nanoid(
                "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
                16
              );
              let targetUpdateResult = await dynamo.update(
                {
                  game_id: {
                    S: gameResult.data.Item.target_game_id.S,
                  },
                },
                {
                  user2_secret: {
                    S: user2Secret,
                  },
                  user2_name: {
                    S: userId,
                  },
                  user2_session_id: { S: sessionId },
                  ttl: {
                    N: ttl,
                  },
                  turn: {
                    N: "1",
                  },
                  p1: {
                    S: String(p1),
                  },
                  p2: {
                    S: String(p1),
                  },
                  updated: {
                    N: moment()
                      .utc()
                      .unix()
                      .toString(),
                  },
                  sequence_id: {
                    N: "0",
                  },
                  turnTime: {
                    N: createTurnTime,
                  },
                }
              );
              if (targetUpdateResult.success) {
                targetUpdateResult.data.secret = user2Secret;
                targetUpdateResult.data.opponent_name =
                  gameResult.data.Item.user1_name.S;
                targetUpdateResult.data.target_game_id =
                  gameResult.data.Item.target_game_id.S;
                targetUpdateResult.data.turnTime = createTurnTimePadded;
                return { data: targetUpdateResult, headers: HEADERS };
              } else {
                return { data: targetUpdateResult, headers: HEADERS };
              }
            } else {
              return { data: updateResult, headers: HEADERS };
            }
          } else {
            //nobody is waiting: update entry
            console.log("nobody is waiting: add entry");

            const user1Secret = nanoid(
              "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
              16
            );
            const targetGameId = nanoid(
              "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
              12
            );
            const version = nanoid("0123456789", 6);
            let ttl = moment()
              .add(30, "s")
              .utc()
              .unix()
              .toString();
            let updateNewResult = await dynamo.updateVC(
              {
                game_id: {
                  S: "L1",
                },
              },
              {
                user1_name: { S: userId },
                user1_secret: { S: user1Secret },
                target_game_id: { S: targetGameId },
                updated: {
                  N: moment()
                    .utc()
                    .unix()
                    .toString(),
                },
                version: { S: version },
                ttl: { N: ttl },
              },
              gameResult.data.Item.version.S
            );
            if (updateNewResult.success) {
              let targetTtl = moment()
                .add(45, "m")
                .utc()
                .unix()
                .toString();
              let targetNewPutResult = await dynamo.put(
                {
                  game_id: {
                    S: targetGameId,
                  },
                  user1_name: { S: userId },
                  user1_secret: { S: user1Secret },
                  user1_session_id: { S: sessionId },
                  updated: {
                    N: moment()
                      .utc()
                      .unix()
                      .toString(),
                  },
                  ttl: { N: targetTtl },
                },
                "attribute_not_exists(game_id)"
              );
              if (targetNewPutResult.success) {
                targetNewPutResult.data.user1Secret = user1Secret;
                targetNewPutResult.data.gameId = targetGameId;
                targetNewPutResult.message = "added";
                return { data: targetNewPutResult, headers: HEADERS };
              } else {
                if (targetNewPutResult.message === "game_id Already Exists") {
                  targetNewPutResult.message = "retry";
                  targetNewPutResult.code = 200;
                }
                return { data: targetPutResult, headers: HEADERS };
              }
            } else {
              return { data: updateNewResult, headers: HEADERS };
            }
          }
        }
      } else {
        return {
          data: {
            success: false,
            message: "Auth Failed",
            code: 401,
          },
          headers: HEADERS,
        };
      }
    }
  }
};

const sendMessage = async (gameId, secret, message) => {
  if (!(gameId && gameId.length >= 6 && gameId.length <= 12)) {
    return {
      data: {
        success: false,
        message: "Invalid GameID",
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  if (!(secret && secret.length === 16)) {
    return {
      data: {
        success: false,
        message: "Invalid Secret",
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  if (!(message && message.length >= 1 && message.length <= 25)) {
    return {
      data: {
        success: false,
        message: "Invalid Message",
        data: {},
      },
      headers: HEADERS,
      status: 200,
    };
  }
  let result = await dynamo.get(gameId);
  console.log(result);
  if (!result.success) {
    if (result.code === 404) {
      return {
        data: {
          success: false,
          message: "Game Doesn't Exist",
          code: 404,
        },
        headers: HEADERS,
      };
    } else {
      return { data: result, headers: HEADERS };
    }
  } else {
    if (
      result.data &&
      result.data.Item &&
      result.data.Item.winner &&
      result.data.Item.winner.N &&
      result.data.Item.winner.N !== ""
    ) {
      return {
        data: {
          success: false,
          message: "Game Already Completed",
          code: 200,
        },
        headers: HEADERS,
      };
    } else if (
      result.data &&
      result.data.Item &&
      result.data.Item.user1_secret &&
      result.data.Item.user1_secret.S &&
      result.data.Item.user2_secret &&
      result.data.Item.user2_secret.S
    ) {
      let sender;
      if (secret === result.data.Item.user1_secret.S) {
        sender = 1;
      } else if (secret === result.data.Item.user2_secret.S) {
        sender = 2;
      } else {
        return {
          data: {
            success: false,
            message: "Unauthorized",
            code: 200,
          },
          headers: HEADERS,
        };
      }
      let objectBody = {};
      if (sender === 1) {
        objectBody.user1_message = { S: message };
        objectBody.user1_message_time = {
          N: moment()
            .utc()
            .unix()
            .toString(),
        };
      } else {
        objectBody.user2_message = { S: message };
        objectBody.user2_message_time = {
          N: moment()
            .utc()
            .unix()
            .toString(),
        };
      }
      let result1 = await dynamo.update(
        {
          game_id: {
            S: gameId,
          },
        },
        objectBody
      );
      if (result1.success && result1.data) {
        return {
          data: {
            success: true,
            message: "Sent",
            code: 200,
          },
          headers: HEADERS,
        };
      } else {
        return {
          data: {
            success: false,
            message: "Something went wrong",
            code: 200,
          },
          headers: HEADERS,
        };
      }
    }
  }
};

//unexposed

const updateUserPoints = async (userId, sessionId, increment) => {
  if (userId && sessionId && increment) {
    let result = await dynamo.userGet(userId);
    if (result.success) {
      if (
        result.data &&
        result.data.Item &&
        result.data.Item.session_id &&
        result.data.Item.session_id.S &&
        result.data.Item.session_id.S === sessionId
      ) {
        let newPoints = 0;
        if (result.data.Item.points && result.data.Item.points.N) {
          newPoints = parseInt(result.data.Item.points.N) + increment;
          await dynamo.userUpdate(
            {
              user_id: {
                S: userId,
              },
            },
            {
              points: { N: String(newPoints) },
            }
          );
        }
      }
    }
  }
  return true;
};
